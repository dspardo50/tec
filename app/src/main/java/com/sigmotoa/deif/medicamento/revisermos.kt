package com.sigmotoa.deif.medicamento

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.deif.medicamento.R

class revisermos : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_revisermos)
        var primera: Button =findViewById(R.id.button)
        var segunda: Button =findViewById(R.id.button2)
        var tercera: Button =findViewById(R.id.button3)
        primera.setOnClickListener { actor1() }
        segunda.setOnClickListener { actor2() }
        tercera.setOnClickListener { actor3() }
    }
    fun actor1(){
        val intent = Intent(this, info::class.java)
        startActivity(intent)
    }
    fun actor2(){
        val intent = Intent(this, diario::class.java)
        startActivity(intent)
    }
    fun actor3(){
        val intent = Intent(this, fisica::class.java)
        startActivity(intent)
    }
}