package com.sigmotoa.deif.medicamento

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.deif.medicamento.R

class sesion : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sesion)
        var primera: Button = findViewById(R.id.button3)
        var primera2: Button = findViewById(R.id.button4)
        primera.setOnClickListener { actor2() }
        primera2.setOnClickListener { actor1() }
    }

    fun actor2() {
        var correo: EditText = findViewById(R.id.editTextTextPersonName)
        if ((correo.text.toString().contains("@")) && (correo.text.toString().contains("."))) {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        } else {
            Toast.makeText(this, "ErrorText", Toast.LENGTH_SHORT).show()
        }
    }
    fun actor1(){
        val intent = Intent(this, registro::class.java)
        startActivity(intent)
    }
}