package com.sigmotoa.deif.medicamento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.deif.medicamento.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var primera: Button = findViewById(R.id.button)
        var primera2: Button = findViewById(R.id.button2)
        var primera3: Button = findViewById(R.id.button34)
        primera.setOnClickListener { actor1() }
        primera2.setOnClickListener { actor2() }
        primera3.setOnClickListener { actor3() }
    }
    fun actor1(){
        val intent = Intent(this, info::class.java)
        startActivity(intent)
    }
    fun actor2(){
        val intent = Intent(this, sesion::class.java)
       startActivity(intent)
    }
    fun actor3(){
        val intent = Intent(this, AboutUs::class.java)
        startActivity(intent)
    }
}